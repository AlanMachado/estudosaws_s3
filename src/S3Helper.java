import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Helper {
	private AmazonS3Client s3;

	public S3Helper() {}
	
	public void connect() throws IOException{
		s3 = new AmazonS3Client(new ProfileCredentialsProvider("default").getCredentials());
		s3.setRegion(Region.getRegion(Regions.SA_EAST_1));
	}
	
	public void listBuckets(){
		System.out.println("Listando buckets");
		for (Bucket bucket : s3.listBuckets()) {
			System.out.println(" - " + bucket.getName());
		}
		System.out.println();
	}
	
	public void listObjects(String bucketName){
		System.out.println("Listing objects: " + bucketName);
	
		ObjectListing objListing = s3.listObjects(new ListObjectsRequest().withBucketName(bucketName));
		
		for(S3ObjectSummary objSummary : objListing.getObjectSummaries()){
			System.out.println(" - " + objSummary.getKey() + " " + "(size = " +objSummary.getSize() + ")");
		}
		System.out.println();
	}
	
	public void createBucket(String bucketName){
		s3.createBucket(bucketName);
	}
	
	public void deleteBucket(String bucketName){
		s3.deleteBucket(bucketName);
	}
	
	public void deleteObject(String bucketName, String objKey){
		s3.deleteObject(bucketName, objKey);
	}
	
	public void putFile(String bucketName, String dir, String fileName,
						String contentType, byte [] bytes) throws IOException{
		
		String path = dir + "/" + fileName;
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType(contentType);
		metadata.setContentLength(bytes.length);
		s3.putObject(bucketName, path, new ByteArrayInputStream(bytes), metadata);
		
		//deixa o arquivo público.
		s3.setObjectAcl(bucketName, path ,CannedAccessControlList.PublicRead);
	
	}
	
	public String getFile(String bucketName, String objKey) throws IOException{
		StringBuffer sb = new StringBuffer();
		S3Object object = s3.getObject(new GetObjectRequest(bucketName, objKey));
		S3ObjectInputStream input = object.getObjectContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		while(true){
			String line = reader.readLine();
			if(line == null)
				break;
			
			sb.append(line);
		}
		
		return sb.toString();
	}
	
	
}
