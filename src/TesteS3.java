import java.io.IOException;

public class TesteS3 {
	public static void main(String[] args) throws IOException{
		S3Helper s3 = new S3Helper();
		s3.connect();
		s3.listBuckets();
		
		String bucketName = "alan-nub-rainbowsix";
		
		s3.createBucket(bucketName);
		s3.listBuckets();
		
		byte [] bytes = "Alan Lucas Machado".getBytes();
		s3.putFile(bucketName, "situacao", "preferidos.txt", "plain/text", bytes);
		s3.listObjects(bucketName);
		
		String objKey = "situacao/preferidos.txt";
		
		String s = s3.getFile(bucketName, objKey);
		
		System.out.println(s);
		
		s3.deleteObject(bucketName, objKey);
	
		s3.deleteBucket(bucketName);
		
		s3.listBuckets();
	}
}
